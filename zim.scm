(define-module (gnu packages zim)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (guix licenses)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages python))

(define-public zim
  (package
   (name "zim")
   (version "0.67")
   (source (origin
            (method url-fetch)
            (uri (string-append "http://zim-wiki.org/downloads/zim-" version ".tar.gz"))
            (sha256
             (base32
              "1gdbzy9qyzj3rn9fsl0ss7lbk9kyka99656bphx2dah694yyyz5k"))))
   (build-system python-build-system)
   (arguments
    `(#:modules ((guix build gnu-build-system)
                 (guix build python-build-system)
                 ((guix build glib-or-gtk-build-system) #:prefix glib-or-gtk:)
                 (guix build utils))
      #:imported-modules (,@%gnu-build-system-modules
                         (guix build python-build-system)
                         (guix build glib-or-gtk-build-system))
      #:python ,python-2
      #:use-setuptools? #f ;; uses distutils rather than setuptools
      #:tests? #f          ;; no 'python setup.py test' command
      #:phases
      (modify-phases %standard-phases
        (add-before 'build 'supply-home-dir
          (lambda _
            ;; Tests require write access to HOME.
            (setenv "HOME" (getcwd)) #t))
        (add-after 'install 'glib-or-gtk-wrap
          (assoc-ref glib-or-gtk:%standard-phases 'glib-or-gtk-wrap)))))
   (inputs
    `(("python2-pygtk" ,python2-pygtk)
      ("python2-pyxdg" ,python2-pyxdg)
      ("desktop-file-utils" ,desktop-file-utils)))
   (synopsis "Zim - A Desktop Wiki Editor")
   (description "")
   (home-page "http://zim-wiki.org/")
   (license gpl2+)))

zim
